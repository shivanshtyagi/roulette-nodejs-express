const express = require('express');
const path = require('path');
const data = require('./MOCK_DATA.json');


const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(express.static('public'));

app.use('/random-topic', (req, res) => {
        const { topics } = data[Math.round(Math.random() * data.length)];
        return res.json({ topics });

});

app.use('/', (req, res) => {
        return res.render('index');
});

app.listen(3000, () => console.log('app listening...'));


